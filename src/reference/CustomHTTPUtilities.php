<?php

class CustomHTTPUtilities extends DefaultHTTPUtilities
{

    public function verifyCSRFToken($token)
    {
        if ($this->getCSRFToken() === $token) {
            return TRUE;
        }
        return FALSE;
    }

    public function setCSRFToken()
    {
        if ($_SESSION['ESAPI']['HTTPUtilities']['CSRFToken'] != '') {
            return null;
        }

        if (!isset($_SESSION)) {
            return null;
        }

        if (!array_key_exists('ESAPI', $_SESSION)) {
            $_SESSION['ESAPI'] = array(
                'HTTPUtilities' => array(
                    'CSRFToken' => ''
                )
            );
        } else if (!array_key_exists('HTTPUtilities', $_SESSION['ESAPI'])) {
            $_SESSION['ESAPI']['HTTPUtilities'] = array(
                'CSRFToken' => ''
            );

        }

        $_SESSION['ESAPI']['HTTPUtilities']['CSRFToken'] = ESAPI::getRandomizer()->getRandomGUID();
    }
}
